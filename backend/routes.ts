import { Express } from "express";
import { createRouter as leaderboard } from "./src/modules/leaderboard/route";
import { createRouter as games } from "./src/modules/games/route";
import { createRouter as userRegistration } from "./src/modules/userRegistration/route";

export class AppRoutes {
  constructor(private server: Express) {}

  public init() {
    // use route
    this.server.use("/live", function(req, res) {
      res.json({ message: "Server Is Running !" });
    });
    this.server.use("/api/v1/leaderboard", leaderboard());
    this.server.use("/api/v1/games", games());
    this.server.use("/api/v1/users", userRegistration());

    this.redirect404();
  }

  private redirect404() {
    // catch 404 and forward to error handler
    this.server.use((req, res) => {
      const err: any = new Error("Not Found");
      err.status = 404;
      res.redirect("/");
    });
  }
}
