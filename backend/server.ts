import * as express from "express";
import * as cors from "cors";
import { urlencoded, json } from "body-parser";
import { get } from "config";
import { ConnectionPool } from "mssql";
import * as Logger from "bunyan";
import "./src/middelware/mix_Middelware/passport";
import { createValidator } from "express-joi-validation";
import settings from "./src/utils/settings";
import { AppRoutes } from "./routes";
import * as helmet from "helmet";
import { Server } from "http";

export const validator = createValidator();

const notRunnableEnvironments = ["test"];

export class App {
  private isInitialized = false;
  private exp = express();

  // NOTE: exposed only for tests
  // if you want to use it in a class pass it via constructor
  public server: Server;
  public routes: AppRoutes;
  public pool: ConnectionPool;
  public log: Logger = Logger.createLogger({ name: "iel-weebpp-api" });

  public getExpress() {
    return this.exp;
  }

  public async initRequirements() {
    this.exp.use(cors());
    this.exp.use(helmet());
    this.exp.use(json({ limit: "3mb" }));
    this.exp.use(urlencoded({ extended: true, limit: "3mb" }));
    // this.exp.use("/static", express.static("public"));

    try {
      this.log.info("Connecting to SQL");
      this.pool = await this.connectSQL();
      this.log.info("Connection to SQL successful");

      // core logger instance
      this.exp.use((_, res, next) => {
        res.locals.log = this.log;
        next();
      });

      // init routes
      this.routes = new AppRoutes(this.exp);

      this.routes.init();
    } catch (e) {
      console.error("Connection to database was not successful");
      throw e;
    }

    // default error handler
    // NOT production error handler we do not want stacktrace leaked to user
    this.exp.use((err, req, res, next) => {
      this.log.error({
        err,
        request: {
          url: req.url,
          body: req.body,
          headers: req.headers,
          method: req.method,
          user: req.user,
        },
      });
      res.status(err.status || 500);
      if (err instanceof Error) {
        res.json(err);
      } else {
        res.send(err);
      }
    });
  }

  private async connectSQL(): Promise<ConnectionPool> {
    return new ConnectionPool(settings.mssql).connect();
  }

  public async start() {
    this.log.info("Initiating requirements ...");
    await this.initRequirements();
    this.log.info(`Starting Express Server `);

    try {
      const port = get("PORT");
      this.log.info(`Starting Express Server ${port}`);
      this.server = this.exp.listen(port);
    } catch (e) {
      throw e;
    }
  }
}

export const app = new App();

if (!notRunnableEnvironments.includes(process.env.NODE_ENV)) {
  app.start();
}
