import { createCipher, createDecipher, randomBytes } from "crypto";
import { get } from "config";

const algorithm = "aes-256-cbc";
const key = get("auth.jwt.encryption.key");
const iv = get("auth.jwt.encryption.iv");
const final = `${key}${iv}`;

// using for encrypt user's password
export function encrypt(text) {
  const cipher = createCipher(algorithm, final);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return encrypted.toString("hex");
}