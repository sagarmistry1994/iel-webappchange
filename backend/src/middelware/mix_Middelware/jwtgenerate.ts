import { sign, verify } from "jsonwebtoken";
import { encrypt } from "../../middelware/encryptions/encryptions";

export function compare(bodypwd, actualpwd) {
  // this function will compare password and genrate jwt token
  const Newpwd = encrypt(bodypwd);
  if (Newpwd === actualpwd) {
    return true;
  }
  return false;
}

export function generateToken(userObj, secret) {
  const abc = sign(userObj, secret);
  return abc;
}

export function generateTokenfor60Min(userObj, secret) {
  const abc = sign(userObj, secret, { expiresIn: "7200000ms" });
  return abc;
}
