import { use } from "passport";
import { Strategy as JWTStrategy, ExtractJwt } from "passport-jwt";
import { get } from "config";

use(
  "iel-gamer-jwt",
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: get("auth.jwt.secret"),
    },
    (jwtPayload, cb) => {
      return cb(null, jwtPayload);
    }
  )
);
