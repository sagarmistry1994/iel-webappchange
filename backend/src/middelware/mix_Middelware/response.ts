export const normalResponse = function (req, res, statuscode: number, result) {
  return res.status(statuscode).json({
    data: result.data,
    message: result.message,
    success: result.success,
    statuscode: statuscode,
  });
};

export const withImageSendResponse = function (
  req,
  res,
  statuscode: number,
  result,
  images
) {
  return res.status(statuscode).json({
    data: result.data,
    message: result.message,
    success: result.success,
    images: images.data,
    statuscode: statuscode,
  });
};

export const internalSererErrorResponse = function (
  req,
  res,
  statuscode: number,
  result
) {
  return res.status(statuscode).json({
    message: result.message,
    statuscode: statuscode,
  });
};
