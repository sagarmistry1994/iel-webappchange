import { send, setApiKey } from "@sendgrid/mail";
import { get } from "config";

export const sendEmail = async (sendTo, Subject, Body, From) => {
  setApiKey(get("sendgrid.key"));
  const msg = {
    to: sendTo,
    from: From,
    subject: Subject,
    html: Body,
  };
  await send(msg)
    .then((result) => {
      // Celebrate
    })
    .catch((error) => {
      // Log friendly error
      console.error(error.toString());

      // Extract error msg
      const { message, code, response } = error;

      // Extract response msg
      const { headers, body } = response;
    });
};
