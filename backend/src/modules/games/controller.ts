import { gameMd } from "./model";
import {
  normalResponse,
  internalSererErrorResponse,
} from "../../middelware/mix_Middelware/response";

export const getAllGames = async (req, res) => {
  // this api use for get all game details
  const result = await gameMd.getGameData();
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};
