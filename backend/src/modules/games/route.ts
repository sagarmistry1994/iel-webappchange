import { Router } from "express";
import { getAllGames } from "./controller";

export const createRouter = () => {
  const router = Router();
  router.get("/all", getAllGames);

  return router;
};
