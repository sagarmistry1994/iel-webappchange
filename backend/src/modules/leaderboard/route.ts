import { Router } from "express";
import {
  PlayerByTournamentId,
  getFeaturedLeaderbord,
  gamewiseLeaderbord,
  newGamewiseLeaderbord,
  newLeaderbord,
} from "./controller";
import { createValidator } from "express-joi-validation";
import {
  tournamentSchema,
  gamewiseLeaderbordSchema,
  newGamewiseLeaderbordSchema,
} from "./req.validation";

export const createRouter = () => {
  const router = Router();
  const validator = createValidator();
  router.get(
    "/tournament/:tournamentId",
    [validator.params(tournamentSchema)],
    PlayerByTournamentId
  );
  router.get("/featuredtournament", getFeaturedLeaderbord);
  router.get(
    "/gamewise/:gameID/:sort",
    [validator.params(gamewiseLeaderbordSchema)],
    gamewiseLeaderbord
  );

  // new updated api 23/04/20 as per new flow
  router.get("/lb", newGamewiseLeaderbord);
  router.get(
    "/lb/:ability/:NoOfRecords",
    [validator.params(newGamewiseLeaderbordSchema)],
    newLeaderbord
  );
  return router;
};
