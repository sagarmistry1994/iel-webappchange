import { Int, VarChar, Bit, Char, Numeric } from "mssql";
import { app } from "../../../server";
import { emailChecker } from "./controller";

class UserRegistration {
  async userNameChecker(userName: any) {
    // this function checking username is available or not
    try {
      const result = await app.pool
        .request()
        .query(
          `select 1 from common.Users u where u.UserName = '${userName}' COLLATE SQL_Latin1_General_Cp1_CS_AS`
        );
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `userName found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `userName Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userProfile(UserID: any) {
    // this function give userProfile data
    try {
      const result = await app.pool.request().query(
        `SELECT u.UserID, u.EmailID , u.UserName, u.ProfileDisplayName, u.ProfilePictureUrl
        , u.FacebookProfile, u.TwitterProfile, u.InstagramProfile
        , u.SteamProfile , u.DiscordProfile, u.XboxProfile, u.PsProfile
        , u.About, igud.InGameUserID, u.SteamID
          FROM common.Users u
          LEFT OUTER JOIN common.InGameUserDetails igud
          ON u.UserID = igud.UserID
          WHERE u.UserID = '${UserID}'`
      );
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `user profile found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `user profile Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userProfileUpdatesteam(UserID: any, SteamID: any) {
    // this function give userProfile data
    try {
      const result = await app.pool.request().query(
        `UPDATE common.Users 
        SET SteamID = '${SteamID}'
        WHERE UserID  = '${UserID}'
        `
      );
      if (result.rowsAffected[0] !== 0) {
        return {
          success: 1,
          statuscode: 200,
          message: `user profile updated`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `user profile Not found`,
        data: result.recordset,
      };
    } catch (err) {
      console.log(err);
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async emailChecker(email: any) {
    // this function checking emailChecker is available or not

    try {
      const result = await app.pool
        .request()
        .query(
          `select 1 from common.Users u where u.EmailID = '${email}' COLLATE SQL_Latin1_General_Cp1_CS_AS`
        );
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `emailID found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `emailID Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userProfileUpdate(data) {
    // this function userProfile data
    try {
      const result = await app.pool
        .request()
        .input("UserID", Int, data.UserID)
        .input("UserName", VarChar, data.UserName)
        .input("ProfileDisplayName", VarChar, data.ProfileDisplayName)
        .input("ProfilePictureUrl", VarChar, data.ProfilePictureUrl)
        .input("FacebookProfile", VarChar, data.FacebookProfile)
        .input("TwitterProfile", VarChar, data.TwitterProfile)
        .input("InstagramProfile", VarChar, data.InstagramProfile)
        .input("SteamProfile", VarChar, data.SteamProfile)
        .input("DiscordProfile", VarChar, data.DiscordProfile)
        .input("XboxProfile", VarChar, data.XboxProfile)
        .input("PsProfile", VarChar, data.PsProfile)
        .input("About", VarChar, data.About)
        .input("InGameUserID", Numeric, data.InGameUserID)
        .execute("common.crud_UsersUpdate");
      if (
        result.recordset.length !== null &&
        result.recordset.length !== 0 &&
        result.recordset[0].msg === "Record updated successfully!"
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `user profile updated `,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `User profile not found! `,
        data: result.recordset,
      };
    } catch (err) {
      console.log(err, "err");
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async countryNames() {
    // this function will give all country names data with id

    try {
      const result = await app.pool
        .request()
        .query(`select CountryID,CountryName from common.Country`);
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `data Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async login(userName: string) {
    // this function will give all country names data with id
    try {
      const result = await app.pool.request()
        .query(`SELECT UserID,EmailID,Pwd FROM common.Users WHERE UserName=ISNULL('${userName}','') COLLATE SQL_Latin1_General_Cp1_CS_AS OR
      EmailID='${userName}'`);
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `data Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async generateOtp(userName: string, flag: string) {
    // this function will generateOtp by userName or emailid
    try {
      const result = await app.pool
        .request()
        .input("userName", VarChar, userName)
        .input("value", Int, flag)
        .execute("common.crud_otp");
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].msgCode === 0
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].msg === "Already email has been verified"
      ) {
        return {
          success: 1,
          statuscode: 409,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `data Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async verifyemail(userName: string, otp: string) {
    // this function will v1erifyemail
    try {
      const result = await app.pool.request().query(`
          UPDATE common.Users SET IsEmailVerified=1,OTP=NULL WHERE (UserName = '${userName}' OR EmailID = '${userName}') AND OTP= '${otp}' 
          SELECT @@ROWCOUNT AS  Result`);
      if (result.recordset.length !== 0 && result.recordset[0].Result !== 0) {
        return {
          success: 1,
          statuscode: 200,
          message: `email verified`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 403,
        message: `email not verified`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async verifyotp(userName: string, otp: string) {
    // this function will verifyotp
    try {
      const result = await app.pool
        .request()
        .query(
          `SELECT 1 FROM common.Users WHERE (UserName = '${userName}' OR EmailID ='${userName}') AND OTP='${otp}'`
        );
      if (result.recordset.length !== 0 && result.recordset[0].Result !== 0) {
        return {
          success: 1,
          statuscode: 200,
          message: `username and otp is correct`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 403,
        message: `username and otp is incorrect`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async changepasword(userName: string, otp: string, password: string) {
    // this function will help to changepasword
    try {
      const result = await app.pool
        .request()
        .input("UserNameEmail", VarChar, userName)
        .input("Pwd", VarChar, password)
        .input("OTP", VarChar, otp)
        .execute("common.crud_UpdatePassword");
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].MsgCode !== 2
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `Password updated successfully `,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 403,
        message: `username and otp is incorrect`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userRegistration(action, EmailID, Pwd) {
    // this function will register user
    try {
      const result = await app.pool
        .request()
        .input("action", VarChar, action)
        .input("EmailID", VarChar, EmailID)
        .input("Pwd", VarChar, Pwd)
        .execute("common.crud_Users");
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].msg !== "Already email id present in the system!"
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      if (
        result.recordset[0].msg === "Already email id present in the system!" &&
        result.recordset[0] !== null
      ) {
        return {
          success: 1,
          statuscode: 409,
          message: result.recordset[0].msg,
          data: result.recordset,
        };
      }
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async verifyemail1(email) {
    // this function will verifyotp
    try {
      const result = await app.pool.request().query(
        `UPDATE common.Users
          SET IsEmailVerified = 1
          WHERE EmailID = '${email}' `
      );
      console.log("result", result);
      if (result.rowsAffected[0] === 1) {
        return {
          success: 1,
          statuscode: 200,
          message: `your email is verified`,
          data: result.recordset,
        };
      }
      return {
        success: 0,
        statuscode: 403,
        message: `username and otp is incorrect`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }
}

export const userRegistrationMD = new UserRegistration();
