import { Router } from "express";
import {
  userNameChecker,
  emailChecker,
  uploadBannerImg,
  countryNames,
  userRegistration,
  login,
  sendmail,
  generateOtp,
  verifyemail,
  verifyotp,
  changepasword,
  userProfile,
  userProfileUpdate,
  verifytoken,
  userProfileUpdatesteam,
} from "./controller";
import { uploadMiddleware } from "../../middelware/aws/aws";
import { createValidator } from "express-joi-validation";
import {
  userNameSchema,
  emailSchema,
  uploadSchema,
  registerUser,
  loginSchema,
  generateSchema,
  verifyEmailSchema,
  verifyOtpAndUserNameSchema,
  changePasswordSchema,
  userProfileSchema,
  userProfileUpdateSchema,
  userProfileUpdateSteamSchema,
} from "./req.validation";
import { authenticate } from "passport";

export const createRouter = () => {
  const router = Router();
  const validator = createValidator();
  router.get(
    "/username/:userName",
    [validator.params(userNameSchema)],
    userNameChecker
  );
  router.get("/email/:email", [validator.params(emailSchema)], emailChecker);
  router.get("/country", countryNames);
  router.post("/register", [validator.params(emailSchema)], emailChecker);
  router.post(
    "/profile/upload",
    [
      validator.body(uploadSchema),
      uploadMiddleware("source", "/images/profile"),
    ],
    uploadBannerImg
  );

  router.post("/otp", [validator.body(generateSchema)], generateOtp);

  router.post("/verifyemail", [validator.body(verifyEmailSchema)], verifyemail);

  router.post(
    "/verifyotp",
    [validator.body(verifyOtpAndUserNameSchema)],
    verifyotp
  );

  router.post(
    "/changepasword",
    [validator.body(changePasswordSchema)],
    changepasword
  );
  router.post("/email", sendmail);

  // new updated api 23/04/20 as per new flow
  router.post(
    "/registration",
    [validator.body(registerUser)],
    userRegistration
  );

  router.post("/login", [validator.body(loginSchema)], login);

  router.get(
    "/userProfile/:UserID",
    authenticate("iel-gamer-jwt", { session: false }),
    [validator.params(userProfileSchema)],
    userProfile
  );

  router.post(
    "/userProfile",
    authenticate("iel-gamer-jwt", { session: false }),
    [validator.body(userProfileUpdateSteamSchema)],
    userProfileUpdatesteam
  );

  router.put(
    "/userProfile",
    authenticate("iel-gamer-jwt", { session: false }),
    [validator.body(userProfileUpdateSchema)],
    userProfileUpdate
  );

  router.get("/token/:token", verifytoken);

  return router;
};
