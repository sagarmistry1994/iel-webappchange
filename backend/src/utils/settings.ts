import { get } from "config";

interface IDatabaseSettings {
  user: string;
  password: string;
  server: string;
  database: string;
  parseJSON: boolean;
}

interface IServerSettings {
  mssql: IDatabaseSettings;
}

const settings: IServerSettings = {
  mssql: get<IDatabaseSettings>("dbConfig.mssql")
};

export default settings;
