/*
 * Animation Loop
 */
jQuery(function () {

	let queuedAnimationNodes = document.getElementsByClassName('animation-queued');
	let allAnimationNodes = document.getElementsByClassName('animation');
	let rect;
	const queue = [];

	setInterval(() => {
		if (queue.length === 0) {
			return;
		}
		const node = queue.shift();
		node.classList.add('animate');
	}, 50);

	const renderLoop = () => {
		// Animate forwards, inside viewport
		for (let node of queuedAnimationNodes) {
			if (node.classList.contains('animation-queued')) {
				rect = node.getBoundingClientRect();

				if ((0 <= rect.top && rect.top <= window.innerHeight * 0.98) || (0 <= rect.bottom && rect.bottom <= window.innerHeight * 0.98)) {
					queue.push(node);
					node.classList.remove('animation-queued');
				}
			}
		}

		// Animate backwards, outside viewport
		for (let node of allAnimationNodes) {
			if (node.classList.contains('animate') && !node.classList.contains('animation-once')) {
				rect = node.getBoundingClientRect();

				// Check if outside viewport
				if (((window.innerHeight * -0.5 <= rect.top && rect.top <= window.innerHeight * 1.5) || (window.innerHeight * -0.5 <= rect.bottom && rect.bottom <= window.innerHeight * 1.5)) === false) {
					// Outside viewport
					node.classList.remove('animate');
					node.classList.add('animation-queued');
				}
			}
		}

		// request new frame to keep the animation going
		requestAnimationFrame(renderLoop);
	};

	// start the render loop
	requestAnimationFrame(renderLoop);

});