import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
// import Home from "./components/Home";
// import About from "./components/About";
import GameDetail from "./components/GameDetail";
import GameLeaderBoard from "./components/GameLeaderBoard";
import Register from "./components/Register";
import NotFound from "./components/NotFound";
import Login from "./components/Login";
import newLogin from "./components/anc";
import UserProfile from "./components/UserProfile";
import VerificationEmail from "./components/VerificationEmail";
import ForgotPassword from "./components/ForgotPassword";

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        {/* <div className="content-body-wrapper"> */}
          <Switch>
            <Route exact path="/leagues" component={GameDetail} />
            <Route exact path="/">
              <Redirect to="/leagues" />
            </Route>
            <Route exact path="/leagues" component={GameDetail} />
            <Route path="/leagues/:id/leaderboard" component={GameLeaderBoard} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/login1" component={Login} />
            <Route exact path="/login" component={newLogin} />
            <Route exact path="/user" component={UserProfile} />
            <Route exact path="/verify-email" component={VerificationEmail} />
            <Route exact path="/forgot-password" component={ForgotPassword} />

            <Route path="" component={NotFound} />
          </Switch>
        {/* </div> */}
        <Footer />
      </div>
    </Router>
  );
}

export default App;
