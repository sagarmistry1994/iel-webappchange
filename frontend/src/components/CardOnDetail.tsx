import React from "react";
import { IFeaturedTournament } from "../utils/interfacePools";
import { Link } from "react-router-dom";

const DetailCard = (props: IFeaturedTournament) => {
  const link = props.TournamentID && props.TournamentID;
  const { category, searchTerm } = props;
  return (
    <>
      <div className="card-on-detail">
        <div className="cod-title">
          {category.title && category.title}
        </div>
        <div className="card-on-detail-body">
        <div className="cod-tb-title">
          <span className="index">#</span>
          <span className="name">Players</span>
          <span className="points"><span>{category && category.dis}</span></span>
        </div>
        <div className="cod-player-tb">
          {props.players && props.players
            ? props.players.map((index, key) => {
                return (
                  <div className={`p-row ${
                    index.PlayerName &&
                    index.PlayerName.toLowerCase()
                      .trim()
                      .indexOf(
                        searchTerm && searchTerm.toLowerCase().trim()
                      ) !== -1
                      ? "red"
                      : "false"
                  }`} key={key}>
                    <div className="index"> {key + 1}</div>
                    <div className="name">
                      <span>{index.PlayerName}</span>
                    </div>
                    <div className="points">
                      {" "}
                      <span>{index[category.column]}</span>
                    </div>
                  </div>
                );
              })
            : null}
        </div>
        
        </div>
        <div className="card-foot">
          <Link
            className="see-more"
            to={{ pathname: `/leagues/${link}/leaderboard` }}
          >
            See full leaderboard
          </Link>
        </div>
      </div>
    </>
  );
};

export default DetailCard;
