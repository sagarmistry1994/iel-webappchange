import React from "react";
import Carousel from "react-bootstrap/Carousel";
import bg from "../assets/bbr2-bg.png";
import bbrLogo from "../assets/bbr2-logo.png";


const CarouselHome = () => {
    return (
      <>
        <div className="">
          <Carousel>
            <Carousel.Item>
              <img className="d-block w-100 carousel-img" src={bg} alt="First slide" />
              <Carousel.Caption>
                <p>
                  <img src={bbrLogo} alt="icon" className="game-logo" />
                </p>
  
                <p className="caption-text">
                  Drive into action-packed off-road kart racing mayhem. Race
                  against a field of rival drivers with unique personalities and
                  special abilities.
                </p>
                <p className="carousel-cta-container">
                    <a className="carousel-cta" href="/">Coming Soon</a>
                </p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img className="d-block w-100 carousel-img" src={bg} alt="First slide" />
              <Carousel.Caption>
                <p>
                  <img src={bbrLogo} alt="icon" className="game-logo" />
                </p>
  
                <p className="caption-text">
                  Drive into action-packed off-road kart racing mayhem. Race
                  against a field of rival drivers with unique personalities and
                  special abilities.
                </p>
                <p className="carousel-cta-container">
                    <a className="carousel-cta" href="/">Coming Soon</a>
                </p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img className="d-block w-100 carousel-img" src={bg} alt="First slide" />
              <Carousel.Caption>
                <p>
                  <img src={bbrLogo} alt="icon" className="game-logo" />
                </p>
  
                <p className="caption-text">
                  Drive into action-packed off-road kart racing mayhem. Race
                  against a field of rival drivers with unique personalities and
                  special abilities.
                </p>
                <p className="carousel-cta-container">
                    <a className="carousel-cta" href="/">Coming Soon</a>
                </p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </div>
      </>
    );
  };
  
  export default CarouselHome;