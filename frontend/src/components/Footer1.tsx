import React from "react";
import NewsletterForm from "./NewsletterForm";

// import fb from './../assets/social-icons/fb.png';
// import twitter from './../assets/social-icons/twitter.png';
// import instagram from './../assets/social-icons/instagram.png';
import logo from "./../assets/white-logo.png";
import { Nav } from "react-bootstrap";

const Footer = () => {
  return (
    <>
      <footer>
        <div className="footer-wrapper">
          <div className="copyright footer-child small-child">
            {/* &copy; Indie Esports League */}
            <Nav.Link href="https://indieesportsleague.com/">
            <img src={logo} alt="IEL"/>
            </Nav.Link>
            &copy; Indie Esports League 2020
            </div>
          <div className="footer-link-wrapper  footer-child small-child">
            <a className="footer-link" href="https://indieesportsleague.com/about/">
              About Us
            </a>
            <a className="footer-link" href="mailto:info@indieesportsleague.com">
              Contact Us
            </a>
            {/* <a className="footer-link" href="/">
              Terms &amp; Conditions
            </a> */}
          </div>
          <div className="footer-link-wrapper footer-child small-child">
            <a className="footer-link" href="https://www.facebook.com/indieesportsleague/" target="_blank" rel="noopener noreferrer">
              <i className="fa fa-facebook"></i>
              <span>Facebook</span>
            </a>
            <a className="footer-link" href="https://twitter.com/indie_league" target="_blank" rel="noopener noreferrer">
            <i className="fa fa-twitter"></i>
            <span>Twitter</span>
            </a>
            <a className="footer-link" href="https://instagram.com/indieesportsleague" target="_blank" rel="noopener noreferrer">
            <i className="fa fa-instagram"></i>
            <span>Instagram</span>
            </a>
          </div>
          <div className="footer-newsletter footer-child">
                <NewsletterForm />
          </div>
          <div className="footer-address footer-child">
                <p className="">500 S Curson Ave, Los Angeles, CA 90036</p>
                <p className=""><a href="tel: +12134221879">+1(213)422 1879</a></p>
                <p className=""><a className="" href="mailto:info@indieesportsleague.com">info@indieesportsleague.com</a></p>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
