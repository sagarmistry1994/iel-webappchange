import React from "react";
import FormTemplate from "./FormTemplate";
import TextInput from "./formFields/TextInput";
import {
  verifyEmailUrl,
  commonHeader,
  changePasswordUrl,
  verifyOtpUrl,
} from "../utils/apis";
import axios from "axios";
import { IVerificationEmail } from "../utils/interfacePools";

import { ErrorMsg } from "../utils/Constants";

type IForgotPasswordProps = any;

interface IForgotPasswordState extends IVerificationEmail {
  userPassword: string;
  userConfirmPassword: string;
  userPasswordError: string;
  confirmPasswordError: string;
  isScreen2: boolean;
}

class ForgotPassword extends React.Component<
  IForgotPasswordProps,
  IForgotPasswordState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      userName: "",
      otp: null,
      userPassword: "",
      userConfirmPassword: "",
      isScreen1: true,
      isScreen2: false,
      userNameErroMsg: "",
      otpErrorMsg: "",
      userPasswordError: "",
      confirmPasswordError: "",
      showSuccessMsg: false,
    };
  }

  handleUserFields = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  handleUser = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.userName) {
      //call api with username redirect to OTP if successfull & display username error msg when not
      this.submitUserName();
    } else {
      this.setState({
        userNameErroMsg: ErrorMsg.userNameEmpty,
        isScreen1: true,
      });
    }
  };

  handleOtp = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.otp) {
      //call api with otp and username, redirect to change password if successful & display OTP error msg when not
      this.submitUserOtp();
    } else {
      this.setState({ otpErrorMsg: ErrorMsg.otpEmpty });
    }
  };

  handlePassword = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!this.state.userPassword) {
      this.setState({ userPasswordError: ErrorMsg.userPasswordEmpty });
    } else if (this.state.userPassword.length < 8) {
      this.setState({ userPasswordError: ErrorMsg.userPasswordInvalid });
    } else if (this.state.userPassword !== this.state.userConfirmPassword) {
      this.setState({
        userPasswordError: "",
        confirmPasswordError: ErrorMsg.userConfirmPassword,
      });
    } else {
      this.setState({
        userPasswordError: "",
        confirmPasswordError: "",
      });
      //Call api, redirect to login if successful, display error if not
      this.submitPassword();
    }
  };

  checkPassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (this.state.userPassword && this.state.userPassword.length < 8) {
      this.setState({ userPasswordError: ErrorMsg.userPasswordInvalid });
    } else if (this.state.userPassword === "") {
      this.setState({ userPasswordError: ErrorMsg.userPasswordEmpty });
    } else {
      this.setState({ userPasswordError: "" });
    }
  };

  checkConfirmPassword = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (this.state.userConfirmPassword === this.state.userPassword) {
      //good to go
      this.setState({ confirmPasswordError: "" });
    } else {
      //show confirmation error
      this.setState({ confirmPasswordError: ErrorMsg.userConfirmPassword });
    }
  };

  async submitUserName() {
    //flag 2 for API - recognizing the username comes from forgot password form
    const data = { userName: this.state.userName.toLowerCase(), flag: 2 };
    let response = await axios
      .post(verifyEmailUrl, data, { headers: commonHeader })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log("error");
        return err;
      });
    if (response.response && response.response.status === 404) {
      this.setState({ userNameErroMsg: ErrorMsg.userNameInvalid });
    } else if (response.response && response.response.status === 409) {
      this.setState({ userNameErroMsg: ErrorMsg.userNameVerified });
    } else if (
      response.data &&
      (response.data.success === 1 || response.data.statuscode === 200)
    ) {
      this.setState({ isScreen2: true, isScreen1: false });
    }
  }

  async submitUserOtp() {
    const data = {
      userName: this.state.userName.toLowerCase(),
      otp: this.state.otp,
    };
    let response = await axios
      .post(verifyOtpUrl, JSON.stringify(data), { headers: commonHeader })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log("error");
        return err;
      });
    if (response.response && response.response.status === 403) {
      this.setState({ otpErrorMsg: ErrorMsg.otpInvalid });
    } else if (
      response.data &&
      (response.data.success === 1 || response.data.statuscode === 200)
    ) {
      this.setState({ isScreen2: false, isScreen1: false });
    }
  }

  async submitPassword() {
    const data = {
      userName: this.state.userName.toLowerCase(),
      otp: this.state.otp,
      password: this.state.userPassword,
    };
    let response = await axios
      .post(changePasswordUrl, JSON.stringify(data), { headers: commonHeader })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log("error");
        return err;
      });
    if (response.response && response.response.status === 403) {
      this.setState({
        confirmPasswordError: "Password could not be updated. Try again!",
      });
    } else if (
      response.data &&
      (response.data.success === 1 || response.data.statuscode === 200)
    ) {
      this.setState({ showSuccessMsg: true });
      setTimeout(() => this.props.history.push("/login"), 1000);
    }
  }

  render() {
    const {
      isScreen1,
      isScreen2,
      otpErrorMsg,
      userNameErroMsg,
      userPasswordError,
      confirmPasswordError,
      showSuccessMsg,
    } = this.state;
    return (
      <>
        {showSuccessMsg ? (
          <div className="registration-success">
            <span className="loading">Password changed successfully</span>
          </div>
        ) : null}
        <div>
          <FormTemplate
            customStyle={true}
            title={
              isScreen1
                ? "Forgot Password"
                : isScreen2
                ? "One Time Password (OTP)"
                : "Change Password"
            }
            subTitle1={"Back to Login"}
            subLink1={"/login"}
            submitBtn1={
              isScreen1
                ? "Get OTP"
                : isScreen2
                ? "Submit OTP"
                : "Change Password"
            }
            onSubmit={
              isScreen1
                ? this.handleUser
                : isScreen2
                ? this.handleOtp
                : this.handlePassword
            }
          >
            {isScreen1 ? (
              <div className="form-group">
                <label className="control-label">
                  Username or Email<span className="required-red">*</span>
                </label>
                <div className="fields-holder">
                  <TextInput
                    name={"userName"}
                    type={"text"}
                    required={"true"}
                    autoComplete={"off"}
                    onChange={this.handleUserFields}
                  />

                  <p className="reg-error-parent">
                    <span className="reg-error">{userNameErroMsg}</span>
                  </p>
                </div>
              </div>
            ) : isScreen2 ? (
              <>
                <p>
                  We have emailed you a One Time Password(OTP), please use the
                  same OTP to verify your account
                </p>
                <div className="form-group">
                  <label className="control-label">
                    Enter OTP<span className="required-red">*</span>
                  </label>
                  <div className="fields-holder">
                    <TextInput
                      name={"otp"}
                      type={"number"}
                      required={"true"}
                      autoComplete={"off"}
                      onChange={this.handleUserFields}
                    />
                    <p className="reg-error-parent">
                      <span className="reg-error">{otpErrorMsg}</span>
                    </p>
                  </div>
                </div>
              </>
            ) : (
              <>
                <div className="form-group">
                  <label className="control-label">
                    New Password<span className="required-red">*</span>
                  </label>
                  <div className="fields-holder">
                    <TextInput
                      name={"userPassword"}
                      type={"password"}
                      required={"true"}
                      autoComplete={"off"}
                      onChange={this.handleUserFields}
                      onBlur={this.checkPassword}
                    />

                    <p className="reg-error-parent">
                      <span className="reg-error">{userPasswordError}</span>
                    </p>
                  </div>
                </div>
                <div className="form-group">
                  <label className="control-label">
                    Confirm Password<span className="required-red">*</span>
                  </label>
                  <div className="fields-holder">
                    <div className="input-icon-wrapper">
                      <input
                        className="form-control"
                        type="password"
                        required={true}
                        name="userConfirmPassword"
                        autoComplete="on"
                        onChange={(e) => this.handleUserFields(e)}
                        onBlur={(e) => this.checkConfirmPassword(e)}
                      />
                    </div>
                    <p className="reg-error-parent">
                      <span className="reg-error">{confirmPasswordError}</span>
                    </p>
                  </div>
                </div>
              </>
            )}
          </FormTemplate>
        </div>
      </>
    );
  }
}

export default ForgotPassword;
