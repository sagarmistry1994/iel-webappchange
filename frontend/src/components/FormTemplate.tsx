import React from "react";
import { Link } from "react-router-dom";

const FormTemplate = (props: any) => {
  return (
    <>
      <div className={`register-wrapper ${props.customStyle ? 'login-margin' : null}`}>
        <div className="container-fluid register-container">
          <div className="col-sm-12 col-xs-12">
            <div className="fixed-strip">
              <p>
                <span>{props.title}</span>
                <span className="sub-section-head">
                  <span className="form-sub-link">
                    <Link to={{ pathname: props.subLink1 }}>
                      {props.subTitle1}
                    </Link>
                  </span>
                  {props.subTitle2 ? (
                    <span className="form-sub-link">
                      <Link to={{ pathname: props.subLink2 }}>
                        {props.subTitle2}
                      </Link>
                    </span>
                  ) : null}
                </span>
              </p>
            </div>
            <div className="register-section-wrapper">
              <form className="form-horizontal">
                {props.children}
                <div className="button-wrapper">
                  <button className="default-btn submit-btn" onClick={(e) => props.onSubmit(e)}>
                    {props.submitBtn1}
                  </button>
                  {props.submitBtn2 ? (
                    <Link className="default-btn trivial-btn" to={{pathname: props.trivialUrl}}>
                      {props.submitBtn2}
                    </Link>
                  ) : null}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormTemplate;
