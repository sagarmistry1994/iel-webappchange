import React from "react";
import { Link } from "react-router-dom";
import cup from "./../assets/cup.png";
import gameImage from "./../assets/shenandoah-logo.png";
import { IFeaturedTournament } from "../utils/interfacePools";


const GameCard = (props: IFeaturedTournament) => {
  const link = props.TournamentID && props.TournamentID;
  return (
    <>
      <div
        className="game-card-wrapper"
        data-id={props.TournamentID ? props.TournamentID : null}
      >
        <div className="game-image">
          <img src={gameImage} className="" alt="game" />
        </div>
        <div className="game-title">
          <img src={cup} className="" alt="cup" />
          {props.TournamentName ? props.TournamentName : null}
        </div>
        <div className="game-player-table">
          {props.players && props.players
            ? props.players.map((index, key) => {
                return (
                  <div className="player-column" key={key}>
                    <div className="player-name">{index.PlayerName}</div>
                    <div className="player-points"> {index.Score}</div>
                  </div>
                );
              })
            : null}
        </div>
        <div className="game-card-footer">
          <Link
            className="game-see-more"
            to={{ pathname: `/game/${props.TournamentID}/leaderboard`, state: { link } }}
          >
            See more
          </Link>
        </div>
      </div>
    </>
  );
};

export default GameCard;
