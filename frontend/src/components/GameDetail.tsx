import React from "react";
// import GameCard from "./GameCardOnDetail";
import DetailCard from "./CardOnDetail";
// import DropdownButton from "react-bootstrap/DropdownButton";
// import Dropdown from "react-bootstrap/Dropdown";
import { IAppState } from "./../reducer";
import { IFeaturedTournament, IPlayers } from "../utils/interfacePools";
import { connect } from "react-redux";
import store from "../store";
import { allLeaderUrl, getMethod } from "../utils/apis";
import { getAllGames } from "../utils/actionDispatcher";
import { gameList } from "../utils/Constants";
import OfflineContent from "./common/OfflineContent";

// import ReactPlayer from "react-player";

// import img1 from "./../assets/all-game-1.png";
// import img2 from "./../assets/all-game-2.png";
// import img3 from "./../assets/all-game-3.png";
// import img4 from "./../assets/all-game-4.png";
// import window from "./../assets/window.png";


// import vid from './../media/IEL-STOMP_main.mp4';
import ScrollToTop from "./common/ScrollToTop";
// import TextInput from "./Form/TextInput";

interface IProps {
  afas?: string;
}

type Props = IProps & LinkStateProps;

interface LinkStateProps {
  allGames: IFeaturedTournament[];
}

interface IState {
  allGames: IFeaturedTournament[];
  sortText: string;
  bgImage: string;
  noData?: boolean;
  isOffline?: boolean;
  searchTerm?: string;
  sendTerm?: string;
  error?: string;
  count?: number;
}

const mapStateToProps = (
  state: IAppState,
  ownProps: IProps
): LinkStateProps => {
  return {
    allGames: state.allGames,
  };
};
class GameDetail extends React.Component<Props, IState> {
  constructor(props: Props) {
    super(props);

    this.state = {
      allGames: props.allGames ? props.allGames : [],
      sortText: "This Year",
      bgImage: "",
    };
  }

  componentDidMount() {
    this.getAllGamesMethod();
  }

  getAllGamesMethod = async (
    gameId: number = 1,
    sortCode: number = 3
  ): Promise<void> => {
    const url = allLeaderUrl;
    // const header = commonHeader;
    let res = await getMethod(url);

    if (res && res.response && res.status === 404) {
      this.setState({ noData: true, isOffline: false });
    } else if (res && res.response && res.status === 500) {
      this.setState({ noData: true, isOffline: false });
    } else if (res && res.data && res.data.statuscode === 200) {
      const response = res.data.data[0];
      store.dispatch(getAllGames(response));
      this.setState({ allGames: response, noData: false, isOffline: false });
    } else if (!res.status) {
      console.log("no response");
      this.setState({ isOffline: true });
    }
  };

  sortCardsMethod(param: number, text: string) {
    this.getAllGamesMethod(1, param);
    this.setState({
      sortText: text,
    });
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchTerm: e.target.value.trim() });
  };

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.searchTerm) {
      this.setState({
        sendTerm: this.state.searchTerm,
        error: "",
      });
      const { allGames, searchTerm } = this.state;
      let data = Object.entries(allGames).map((item) =>
        item[1][0].players.filter((index: IPlayers) =>
          index.PlayerName.toLowerCase()
            .trim()
            .includes(searchTerm && searchTerm.toLowerCase().trim())
        )
      );
      var count = data.filter((ind) => ind.length > 0);
      if (count && count.length) {
        this.setState({ count: count.length });
      } else {
        this.setState({ error: "nothing found" });
      }
    } else {
      this.setState({
        error: "search is empty",
        sendTerm: this.state.searchTerm,
      });
    }
  };

  render() {
    const { allGames, isOffline } = this.state;
    return (
      <>
        <div className="game-bg-wrapper">
          <div className="game-bg-holder all-boards">
            <div className="component-inner">
              <div className="introsection">
                <h1>1993 Shenandoah</h1>
                <div className="buybutton">
                  <label className="getgamelabel">GET GAME <span></span></label>
                  <ul className="buyerdrop">
                    <li><a href="https://store.steampowered.com/app/373480/1993_Space_Machine/" target="_blank">STEAM</a></li>
                    <li><a href="#" target="_blank">XBOX</a></li>
                    <li><a href="#" target="_blank">PLAYSTATION</a></li>
                    <li><a href="#" target="_blank">EPIC GAME STORE</a></li>
                    <li><a href="#" target="_blank">STADIA</a></li>
                    <li><a href="#" target="_blank">GOG</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="component-inner carousel-container">
              <div className="carousel" data-flickity='{ "cellAlign": "left", "contain": true, "freeScroll": false, "wrapAround": true, "imagesLoaded": true }'>

                <div className="carousels-item video">
                  <img src={require('../media/game-videopreview-1993shenandoah.jpg')} />
                  <div className="overlay video-play"></div>
                </div>

                <div className="carousels-item image">
                  <img src={require('../media/homepage-cta-developers.jpg')} />
                </div>
              </div>
            </div>
            <div className="component-inner game-meta">

              <div className="publisher">
                <h3 className="publisher-name">LIMIT BREAK STUDIOS</h3>
                <p className="publisher-type">ARCADE SPACE SHOOTER</p>
              </div>

              <ul className="platforms">
                <li className="platform-item">
                  <div className="format-icon steam"></div>
                </li>
                <li className="platform-item">
                  <div className="format-icon xbox"></div>
                </li>
                <li className="platform-item">
                  <div className="format-icon windows"></div>
                </li>
              </ul>
            </div>
            {/* <div className="side-holder">
              <div className="side-text">
                <div>
                  <div className="lg-txt">1993</div>
                  <div className="lg-txt">Shenandoah</div>
                  <div className="sm-txt bottom-txt">Limit break Studio</div>
                </div>
                <div>
                  <div>
                    <a
                      href="http://store.steampowered.com/app/373480/"
                      target="blank"
                      className="register-btn"
                    >
                      GET GAME
                    </a>
                  </div>
                </div>
                
              </div>
            </div> */}
          </div>
        </div>
        <ScrollToTop />
        <div className="home-wrapper game-leader-bg component-leaderboards">
          <div className="container home-container game-detail-container">
            {isOffline ? <OfflineContent /> : null}
            <p className="leader-title">LEADERBOARDS</p>
            <div className="platform-selection">
            <ul className="tabs">
              <li className="tab-item"><a href="#">PC</a></li>
              <li className="tab-item active"><span>Playstation</span></li>
              <li className="tab-item"><a href="#">Xbox</a></li>
              <li className="tab-item"><a href="#">Switch</a></li>
              <li className="tab-item"><a href="#">iOS</a></li>
              <li className="tab-item"><a href="#">Android</a></li>
            </ul>
          </div>
            {/* <div className="id-search-wrap"> */}
              {/* <div>
                <div className="form-head">What's your rank?</div>
                <form onSubmit={this.handleSubmit}>
                  <TextInput
                    onChange={this.handleChange}
                    error={this.state.error}
                    placeholder="Enter user ID"
                    value={this.state.searchTerm && this.state.searchTerm}
                  />
                  <button>Ok</button>
                </form>
              </div> */}
            {/* </div> */}
            <div className="row">
              {Object.entries(allGames).map((index: any, key: any) => {
                return (
                  <div
                    className="col-lg-4 col-md-6 col-sm-12 col-xs-12"
                    key={key}
                  >
                    <DetailCard
                      // TournamentName={index[1][0].Name}
                      players={index[1][0].players}
                      category={gameList[key]}
                      TournamentID={key + 1}
                      searchTerm={
                        this.state.sendTerm ? this.state.sendTerm : "null"
                      }
                    />
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default connect(mapStateToProps, {})(GameDetail);
