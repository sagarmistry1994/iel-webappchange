import React from "react";
import { RouteComponentProps } from "react-router-dom";
import { connect } from "react-redux";
import { ILeaderBoardRow } from "../utils/interfacePools";
import { IAppState } from "./../reducer";

import { getLeaderBoardDetails } from "../utils/actionDispatcher";
import { getMethod, allLeaderUrl } from "../utils/apis";
import LeaderBoardList from "./LeaderBoardList";
import store from "../store";
import { gameList } from "../utils/Constants";
import ScrollToTop from "./common/ScrollToTop";
import TextInput from "./Form/TextInput";
interface IProps {
  afas?: string;
}

interface IGameLeaderBoardProps extends RouteComponentProps<{ id: string }> {}

type Props = IProps & IGameLeaderBoardProps & LinkStateProps;

interface LinkStateProps {
  leaderBoard: ILeaderBoardRow[];
}

interface IState {
  leaderBoard: ILeaderBoardRow[];
  gameBg: string;
  featuredPlayerBg: string;
  showLoading?: boolean;
  noData?: boolean;
  searchTerm?: string;
  sendTerm?: string;
  error?: string;
  count?: number;
}

const mapStateToProps = (
  state: IAppState,
  ownProps: IProps
): LinkStateProps => {
  return {
    leaderBoard: state.leaderBoard,
  };
};

class GameLeaderBoard extends React.Component<Props, IState> {
  constructor(props: Props) {
    super(props);
    this.state = {
      leaderBoard: props.leaderBoard ? props.leaderBoard : [],
      gameBg: "",
      featuredPlayerBg: "",
      sendTerm: "",
      searchTerm: "",
    };
  }
  componentDidMount() {
    this.getLeaderBoardMethod();
  }

  getLeaderBoardMethod = async (): Promise<void> => {
    this.setState({ showLoading: true });

    const currentUrl = this.props.match.params.id;

    const url = `${allLeaderUrl}/${currentUrl}/50`;
    let res = await getMethod(url);

    if (res && res.response && res.response.status === 404) {
      this.setState({ noData: true, showLoading: false });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ showLoading: false });
    } else if (res && res.data && res.data.statuscode === 200) {
      const response = res.data.data;
      store.dispatch(getLeaderBoardDetails(response));
      this.setState({
        leaderBoard: this.props.leaderBoard,
        noData: false,
        showLoading: false,
      });
    }
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchTerm: e.target.value.trim() });
  };

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.searchTerm) {
      this.setState({
        sendTerm: this.state.searchTerm,
        error: "",
      });
      const { leaderBoard, searchTerm } = this.state;
      let data = leaderBoard.filter(
        (item) =>
          item.ProfileDisplayName &&
          item.ProfileDisplayName.toLowerCase()
            .trim()
            .includes(searchTerm && searchTerm.toLowerCase().trim())
      );
      if (data && data.length) {
        this.setState({ count: data.length });
      } else {
        this.setState({ error: "nothing found" });
      }
    } else {
      this.setState({
        error: "search is empty",
        sendTerm: this.state.searchTerm,
      });
    }
  };

  // clearSearch = () => {
  //   this.setState({ searchTerm: "", sendTerm: "", error: "" });
  // };

  render() {
    return (
      <>
        <div className="game-bg-wrapper">
          <div className="game-bg-holder detail-board"></div>
        </div>
        <ScrollToTop />
        <div className="home-wrapper game-leader-bg">
          <div className="container home-container">
            <div className="row">
              <div className="col-sm-12 col-xs-12">
                <div className="game-card-leaderboard-wrapper">
                  <p className="leader-title">
                    <span>
                      {gameList[parseInt(this.props.match.params.id) - 1].title}
                    </span>{" "}
                    LEADERBOARD
                  </p>
                  <div className="id-search-wrap">
                    <div>
                      <div className="form-head">What's your rank?</div>
                      <form onSubmit={this.handleSubmit}>
                        <TextInput
                          onChange={this.handleChange}
                          error={this.state.error}
                          placeholder="Enter user ID"
                          value={this.state.searchTerm && this.state.searchTerm}
                        />
                        <button>Ok</button>
                      </form>
                    </div>
                    {/* <div className="clear-div" onClick={this.clearSearch}>Clear</div> */}
                  </div>

                  {this.state.noData ? (
                    <p>No data found</p>
                  ) : (
                    <LeaderBoardList
                      list={this.state.leaderBoard}
                      loading={this.state.showLoading}
                      codeId={parseInt(this.props.match.params.id) - 1}
                      searchTerm={
                        this.state.sendTerm ? this.state.sendTerm : "null"
                      }
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* <div className="featured-player-wrapper">
          <div
            className="featured-player-container"
            style={{
              backgroundImage:
                this.state.featuredPlayerBg &&
                `url("${this.state.featuredPlayerBg}")`,
            }}
          >
            <div className="container content-container">
              <div className="col-sm-12 col-xs-12">
                <div className="featured-player-name">
                  Featured player - TINTIN
                </div>
                <div className="featured-player-desc">
                  A true pro-athlete in the making! TinTin has been consistently
                  ranking through the league since its inception. She's a
                  powerhouse of combo madness.
                </div>
              </div>
            </div>
          </div>
        </div> */}
      </>
    );
  }
}

export default connect(mapStateToProps, {})(GameLeaderBoard);
