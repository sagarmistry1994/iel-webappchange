import React from "react";
import { Link } from "react-router-dom";
import gameImage from "./../assets/shenandoah-logo.png";
import { IOurGameCardOnHome } from "../utils/interfacePools";

const GameSummaryCard = (props: IOurGameCardOnHome) => {
  return (
    <>
      <div className="og-game-card-wrapper" data-id={props.GameID}>
        <div className="game-image">
          <img src={gameImage} alt="game" className="" />
        </div>
        <div className="game-content">
          <div className="title">{props.GameName && props.GameName}</div>
          <div className="desc">{props.GameDesc && props.GameDesc}</div>
          {/* <a className="cta" href="/games">
            Browse Games
          </a> */}
          <Link className="cta" to={{ pathname: "/games" }}>
            Browse Games
          </Link>
        </div>
      </div>
    </>
  );
};

export default GameSummaryCard;
