import React from 'react';

import logo from '../images/global-header-logo.png';

const Header = (props: any) => {
    return (
        <div id="header">
	<div className="header-inner">
		<a className="header-logo" href="/">
			<img aria-hidden="true" src={logo} alt="images/global-header-logo.png" />
			<span className="accessible">Indie Esports League</span>
		</a>

		<div className="header-nav">
			<ul className="header-nav-primary" role="navigation">
				<li className="nav-item">
					<a href="games.html">Games</a>
				</li>
				<li className="nav-item">
					<a href="about.html">About</a>
				</li>
				<li className="nav-item mobile-only">
					<a href="contact.html">Contact</a>
				</li>
				<li className="nav-item account">
					<span>
						<a href="account-login.html">Log in</a> /
						<a href="account-create.html">Sign Up</a>
					</span>
					<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
						<path id="global-header-account"
						      d="M0,50V43.751c0-6.876,11.25-12.5,25-12.5s25,5.625,25,12.5V50ZM12.5,12.5A12.5,12.5,0,1,1,25,25,12.5,12.5,0,0,1,12.5,12.5Z"
						      fill="#fff"/>
					</svg>
				</li>
			</ul>
		</div>

		<button className="header-nav-toggle" type="button">
            <span className="toggle-box">
                <span className="toggle-inner"></span>
            </span>
		</button>
	</div>
</div>
    );
};


export default Header;