import React from "react";
import logo from "./../assets/white-logo.png";
// import stickyLogo from "./../assets/iel-text-logo.svg";
import { IUserProfileNewDetails } from "../utils/interfacePools";
import { IAppState } from "../reducer";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { Nav, Navbar } from "react-bootstrap";
import {
  IsUserProfileLocalStorage,
  getLocalStorage,
} from "../utils/commonUtils";
import Constants from "../utils/Constants";

interface headerState {
  showBreadCrumb: boolean;
  isUserLoggedIn?: boolean;
  showPopUp?: boolean;
  userID?: number;
}

interface LinkProps {
  user: IUserProfileNewDetails;
}
type Prop = any;

type IProps = Prop & LinkProps;

const mapStateToProps = (state: IAppState, ownProps: Prop): LinkProps => {
  return {
    user: state.user,
  };
};

class Header extends React.Component<IProps, headerState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      showBreadCrumb: false,
      showPopUp: false,
      userID: this.props.user.UserID && this.props.user.UserID,
    };
  }

  componentDidMount() {
    // window.addEventListener("scroll", this.resizeHeaderOnScroll);
    const a = IsUserProfileLocalStorage();
    if (a) {
      this.setState({ userID: a });
    } else {
      let b = this.props && this.props.user && this.props.UserID;
      this.setState({ userID: b });
    }
  }
  // resizeHeaderOnScroll() {
  //   const distanceY = window.pageYOffset || document.documentElement.scrollTop,
  //     shrinkOn = 100,
  //     headerEl = document.getElementById("js-header") as HTMLElement;

  //   if (distanceY > shrinkOn) {
  //     headerEl.classList.add("smaller");
  //   } else {
  //     headerEl.classList.remove("smaller");
  //   }
  // }

  componentWillUnmount() {
    // window.removeEventListener("scroll", this.resizeHeaderOnScroll);
  }

  togglePopUp = () => {
    this.setState({
      showPopUp: !this.state.showPopUp,
    });
  };

  onClose = () => {
    this.setState({
      showPopUp: false,
    });
  };

  onProfileHandle = () => {
    this.onClose();
    // this.props.history.push("/user")
  };

  render() {
    const { showBreadCrumb } = this.state;
    const a = this.props.user && this.props.user.UserID;
    const b = IsUserProfileLocalStorage();
    const c = getLocalStorage(Constants.userToken);

    let d: any = "";
    if ((a || b) && c) {
      d = true;
    } else {
      d = false;
    }
    return (
      <>
        <header id="js-header">
          <Navbar
            className="header-container"
            collapseOnSelect
            expand="lg"
            bg="dark"
            variant="dark"
          >
            <Navbar.Brand className="visible-mb">
              <Nav.Link eventKey={1} href="https://indieesportsleague.com/">
                <img src={logo} alt="logo" />
              </Nav.Link>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav" className="head-nav">
              <Nav className="no-content"></Nav>
              <Nav className="visible-dp">
                <Nav.Link href="https://indieesportsleague.com/">
                  <img src={logo} alt="logo" />
                </Nav.Link>
              </Nav>
              <Nav className="custom-nav">
                <div>
                  <Nav.Link
                    eventKey={2}
                    as={Link}
                    to={{ pathname: "/leagues" }}
                    className="header-link no-bd"
                  >
                    Leagues
                  </Nav.Link>
                  <Nav.Link
                    eventKey={3}
                    href="https://indieesportsleague.com/about/"
                    className="header-link no-bd"
                  >
                    About
                  </Nav.Link>
                  <Nav.Link
                    eventKey={4}
                    href="https://indieesportsleague.com/games/"
                    className="header-link no-bd"
                  >
                    Games
                  </Nav.Link>
                </div>
                {/* <Link to={{ pathname: "/games" }} className="header-link no-bd">
                  About
                </Link>
                <Link to={{ pathname: "/games" }} className="header-link no-bd">
                  Games
                </Link> */}
                {d ? (
                  <Nav.Link
                    eventKey={5}
                    as={Link}
                    to={{ pathname: `/user` }}
                    className="header-link no-bd"
                  >
                    My Account
                  </Nav.Link>
                ) : (
                  <Nav.Link
                    eventKey={6}
                    as={Link}
                    to={{ pathname: "/login" }}
                    className="header-link header-link-button"
                  >
                    Sign In
                  </Nav.Link>
                )}
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          {showBreadCrumb ? (
            <div className="bread-crumb">
              <div className="link-container">
                <a className="bread-crumb-link" href="/">
                  Game Name
                </a>
                <a className="bread-crumb-link" href="/">
                  Leaderboard
                </a>
              </div>
            </div>
          ) : null}{" "}
        </header>
      </>
    );
  }
}

export default connect(mapStateToProps, {})(Header);
