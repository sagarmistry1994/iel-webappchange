import React from "react";
import { Link } from "react-router-dom";
import CarouselHome from "./CarouselHome";
import gg from "./../assets/Group 548.png";
import GameCard from "./GameCardOnHome";
import ggg from "./../assets/Group 549.png";
import GameSummaryCard from "./GameSummaryCard";
import { connect } from "react-redux";
import axios from "axios";
import store from "../store";
import { getFeaturedTournaments, getOwnGames } from "../utils/actionDispatcher";
import {
  featuredTournamentUrl,
  commonHeader,
  allGamesUrl,
} from "../utils/apis";
import {
  IFeaturedTournament,
  IOurGameCardOnHome,
} from "../utils/interfacePools";

import { IAppState } from "./../reducer";
interface IHomeProps {
  abc?: string;
  asf?: string;
}
interface IHomeState {
  featuredTournaments: IFeaturedTournament[];
  ourGames: IOurGameCardOnHome[];
  tournamentsLoaded: boolean,
}

interface LinkStateProps {
  featuredTournaments: IFeaturedTournament[];
  ourGames: IOurGameCardOnHome[];
}

const mapStateToProps = (
  state: IAppState,
  ownProps: IHomeProps
): LinkStateProps => {
  return {
    featuredTournaments: state.featuredTournaments,
    ourGames: state.ourGames,
  };
};

type Props = IHomeProps & LinkStateProps;
class Home extends React.Component<Props, IHomeState> {
  constructor(props: Props) {
    super(props);
    this.state = {
      featuredTournaments: props.featuredTournaments,
      ourGames: props.ourGames,
      tournamentsLoaded: false,
    };
  }

  componentDidMount() {
    this.getFeaturedTournamentsMethod();
    this.getOurGamesMethod();
  }

  getFeaturedTournamentsMethod() {
    const url = featuredTournamentUrl;
    const header = commonHeader;
    axios
      .get(url, { headers: header })
      .then(res => {
        if (res.data.success === 1 || res.data.statuscode === 200) {
          const response = res.data.data[0];
          store.dispatch(getFeaturedTournaments(response));
        }
      })
      .catch(err => {
        console.log(err);
      });
      this.setState({
        featuredTournaments: this.props.featuredTournaments,
        ourGames: this.props.ourGames,
        tournamentsLoaded: true
      })
  }

  getOurGamesMethod() {
    const url = allGamesUrl;
    const header = commonHeader;
    axios
      .get(url, { headers: header })
      .then(res => {
        if (res.data.success === 1 || res.data.statuscode === 200) {
          const response = res.data.data;
          store.dispatch(getOwnGames(response));
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <>
        <CarouselHome />
        <div className="home-wrapper">
          <div className="container home-container">
            <div className="home-leaderboard-wrapper">
              <div className="row">
                <div className="home-heading-bg">
                  <img
                    src={gg}
                    alt="featured leaderboard"
                    className="home-btn-wrapper"
                  />
                </div>
              </div>
              <div className="row">
                {this.props.featuredTournaments &&
                this.props.featuredTournaments
                  ? this.props.featuredTournaments.map((index, key) => {
                      return (
                        <div
                          className="col-lg-3 col-md-6 col-sm-6 col-xs-12"
                          key={key}
                        >
                          <GameCard
                            TournamentName={index.TournamentName}
                            TournamentID={index.TournamentID}
                            players={index.players}
                          />
                        </div>
                      );
                    })
                  : null}
              </div>
              <div className="row">
                <p className="home-leaderboard-cta-wrapper">
                  <Link className="home-leaderboard-cta" to={{pathname: '/games'}}>
                    Browse Games
                  </Link>
                </p>
              </div>
            </div>
            <div className="home-our-games-wrapper">
              <div className="row">
                <div className="home-heading-bg">
                  <img src={ggg} alt="our games" className="home-btn-wrapper" />
                </div>
              </div>
              <div className="row">
                {this.props.ourGames && this.props.ourGames
                  ? this.props.ourGames.map((index, key) => {
                      return (
                        <div
                          className="col-lg-6 col-md-6 col-sm-12 col-xs-12"
                          key={key}
                        >
                          <GameSummaryCard
                            GameName={index.GameName}
                            GameDesc={index.GameDesc}
                            GameID={index.GameID}
                            GameShortDesc={index.GameShortDesc}
                          />
                        </div>
                      );
                    })
                  : null}
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default connect(mapStateToProps, {})(Home);
