import React from "react";
// import Table from "react-bootstrap/Table";
import { ILeaderBoardRow } from "../utils/interfacePools";
import { gameList } from "../utils/Constants";
import LoadingData from "./common/LoadingData";

interface IListProps {
  list?: ILeaderBoardRow[];
  loading?: boolean;
  codeId: number;
  searchTerm: string;
}

function LeaderBoardList(props: IListProps) {
  const category = gameList;

  const { list, codeId, loading, searchTerm } = props;

  // let data =
  //   list &&
  //   list.filter(
  //     (item) =>
  //       item.ProfileDisplayName &&
  //       item.ProfileDisplayName.toLowerCase().trim().includes(searchTerm && searchTerm.toLowerCase().trim())
  //   );


  return (
    <>
      <LoadingData isLoading={loading} />
      <div className="table-responsive">
        <table className="table table-bordered leaderboard-list">
          <thead>
            <tr>
              <th>Rank</th>
              <th>Player</th>
              <th>{list && category[codeId].title}</th>
            </tr>
          </thead>
          <tbody>
            {list &&
              list.map((list, key) => {
                return (
                  <tr
                    key={key}
                    className={`${
                      list.ProfileDisplayName &&
                      list.ProfileDisplayName.toLowerCase()
                        .trim()
                        .indexOf(
                          searchTerm && searchTerm.toLowerCase().trim()
                        ) !== -1
                        ? "red"
                        : "false"
                    }`}
                  >
                    {/* <tr key={key}> */}
                    <td>{key + 1}</td>
                    <td>{list.ProfileDisplayName}</td>
                    <td>{list[`${category[codeId].detailCol}`]}</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default LeaderBoardList;
