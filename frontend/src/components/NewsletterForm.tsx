import React from "react";

const NewsletterForm = () => {
  const onsubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
  };
  return (
    <>
      <form className="newsletter-form" onSubmit={onsubmit}>
        <p>Subscribe to our newsletter</p>
        <div className="newsletter">
          <input
            className="form-control"
            type="email"
            placeholder="Email Address"
          />
          <button>OK</button>
        </div>
      </form>
    </>
  );
};

export default NewsletterForm;
