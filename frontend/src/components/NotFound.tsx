import React from "react";

const NotFound = () => (
  <div className="container">
    <div className="col-sm-12 col-xs-12" style={{ marginTop: "100px", marginBottom: "100px", display: "flex", justifyContent: "center", alignItems: "center", flexDirection: "column" }}>
      <h3>404 page not found</h3>
      <p>We are sorry but the page you are looking for does not exist.</p>
    </div>
  </div>
);

export default NotFound;
