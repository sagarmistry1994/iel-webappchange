import React from "react";

import fb from "./../assets/social-icons/fb.png";
import twitter from "./../assets/social-icons/twitter.png";
import instagram from "./../assets/social-icons/instagram.png";
import ps from "./../assets/playstation.png";
import steam from "./../assets/steam.png";
import discord from "./../assets/discord.png";
import xbox from "./../assets/xbox-one.png";
import { IUserProfileNewDetails } from "../utils/interfacePools";

interface IProps {
  details?: IUserProfileNewDetails;
}

function ProfileDetails(props: IProps) {
  const { details } = props;
  return (
    <>
      <div className="usr-left-sec">
        <div className="usr-personal">
          <div className="usr-gamerid">
            {details && details.UserName ? details.UserName : `GamerId`}
          </div>
          <div className="usr-name">
            {details && details.ProfileDisplayName
              ? details.ProfileDisplayName
              : `Rich Thomas`}
          </div>
          <div className="usr-loc">
            <i className="fa fa-globe"></i>CA, USA
          </div>
          <div className="usr-social">Social</div>
          <div className="usr-soc-links">
            <a
              className=""
              href={`${
                details && details.FacebookProfile
                  ? details.FacebookProfile
                  : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={fb} className="fb" alt="facebook" />{" "}
            </a>
            <a
              className=""
              href={`${
                details && details.TwitterProfile ? details.TwitterProfile : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={twitter} className="tw" alt="twitter" />
            </a>
            <a
              className=""
              href={`${
                details && details.InstagramProfile
                  ? details.InstagramProfile
                  : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              {" "}
              <img src={instagram} className="ig" alt="instagram" />
            </a>
          </div>
        </div>
        {/* {details &&
        (details.SteamProfile ||
          details.XboxProfile ||
          details.PsProfile ||
          details.DiscordProfile) ? ( */}
        <div className="usr-soc-profiles">
          <div className="usr-social">PLATFORM ID's</div>
          <a
            className="usr-social-link"
            href={`${
              details && details.SteamProfile ? details.SteamProfile : ""
            }`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={steam} alt="steam" /> Steam Profile
          </a>
          <a
            className="usr-social-link"
            href={`${
              details && details.DiscordProfile ? details.DiscordProfile : ""
            }`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={discord} alt="discord" /> Discord Profile
          </a>
          <a
            className="usr-social-link"
            href={`${details && details.PsProfile ? details.PsProfile : ""}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={ps} alt="ps" /> Ps Profile
          </a>
          <a
            className="usr-social-link"
            href={`${
              details && details.XboxProfile ? details.XboxProfile : ""
            }`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src={xbox} alt="xbox" /> Xbox Profile
          </a>
        </div>
        {/* ) : null} */}
      </div>
    </>
  );
}

export default ProfileDetails;
