import React from "react";
import game1 from "./../assets/static-cover.png";
import { IUserProfileNewDetails } from "../utils/interfacePools";

interface Iprops{
  details?: IUserProfileNewDetails
}
function ProfileGameDetails(props: Iprops) {
  return (
    <>
      <div className="usr-right-sec">
        <div className="usr-title">Your Games, leagues and stats</div>
        <div className="tile-container">
          <div className="game-tile">
            <div className="game-img">
              <img src={game1} alt="" />
            </div>
            <div className="game-head">speedrun 25</div>
            <div className="game-others">
              <div>3rd position</div>
              <div>2394</div>
              <div>134 its clear of 4th</div>
            </div>
          </div>
          <div className="game-tile">
            <div className="game-img">
              <img src={game1} alt="" />
            </div>
            <div className="game-head">speedrun 25</div>
            <div className="game-others">
              <div>3rd position</div>
              <div>2394</div>
              <div>134 its clear of 4th</div>
            </div>
          </div>
          <div className="game-tile">
            <div className="game-img">
              <img src={game1} alt="" />
            </div>
            <div className="game-head">speedrun 25</div>
            <div className="game-others">
              <div>3rd position</div>
              <div>2394</div>
              <div>134 its clear of 4th</div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ProfileGameDetails;
