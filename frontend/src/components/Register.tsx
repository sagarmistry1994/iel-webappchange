import React from "react";
// import { IRegisterState } from "../utils/interfacePools";
import Constants, { errorMsg } from "./../utils/Constants";
import { userRegistrationUrl, postMethod } from "../utils/apis";
import { Link } from "react-router-dom";
import TextInput from "../components/Form/TextInput";
import {
  handleEmail,
  handlePassword,
  handleConfirmPassword,
} from "../components/Form/FormValdiations";
import { removeFromLocalStorage } from "../utils/commonUtils";
import store from "../store";
import { userLogOut } from "../utils/actionDispatcher";

type IRegisterProps = any;

// const UserFields = Constants.UserFields;

export interface IErrors {
  [key: string]: string;
}
export interface IRegisterState {
  errors: IErrors;
  submitted?: boolean;
  email: string;
  password: string;
  confirmpassword: string;
  errorFromApi?: string;
  [key: string]: any;
  btnLoader?: boolean;
}

class Register extends React.Component<IRegisterProps, IRegisterState> {
  constructor(props: IRegisterProps) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      email: "",
      password: "",
      confirmpassword: "",
      errorFromApi: "",
    };
  }

  componentDidMount() {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
  }

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  private haveErrors(errors: IErrors) {
    let haveError: boolean = false;
    Object.keys(errors).forEach((key: string) => {
      if (errors[key].length > 0) {
        haveError = true;
      }
    });
    return haveError;
  }

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const { email, password, confirmpassword } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          email: handleEmail(email),
          password: handlePassword(password),
          confirmpassword: handleConfirmPassword(confirmpassword, password),
        },
      },
      () => {
        if (!this.haveErrors(this.state.errors)) {
          const form = { emailId: email.toLowerCase(), password };
          this.submitCall(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitCall = async (param: object) => {
    this.setState({ btnLoader: true });
    let res = await postMethod(userRegistrationUrl, param);

    if (res && res.response && res.response.status === 404) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.forbidden });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.registered });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.postRegister();
    }
  };

  postRegister = () => {
    this.setState({ btnLoader: false, submitted: true }, () => {
      setTimeout(() => {
        this.props.history.push("/login");
      }, 1000);
    });
  };

  render() {
    return (
      <>
        <div className="register-wrapper">
          <div className="container-fluid register-container">
            <div className="col-sm-12 col-xs-12">
              <div className="register-section-wrapper">
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                  <p className="login-reg-title">REGISTER</p>
                  {/* {this.state.submitted ? (
                    <div className="form-success">
                      <span className="loading"> Registration successful </span>
                    </div>
                  ) : null} */}
                  <TextInput
                    placeholder="YOUR EMAIL"
                    type="email"
                    onChange={this.handleChange}
                    name={"email"}
                    error={this.state.errors["email"]}
                    autoComplete="off"
                  />
                  <TextInput
                    placeholder="YOUR PASSWORD"
                    type="password"
                    onChange={this.handleChange}
                    error={this.state.errors["password"]}
                    name={"password"}
                    autoComplete="off"
                  />
                  <TextInput
                    placeholder="REPEAT PASSWORD"
                    type="password"
                    onChange={this.handleChange}
                    name={"confirmpassword"}
                    error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                  />
                  <div
                    className="reg-error-parent"
                    style={{ marginBottom: "10px" }}
                  >
                    <p className="reg-error">{this.state.errorFromApi}</p>
                  </div>
                  <div className="form-group">
                    <button className="register-btn full-width">
                      {" "}
                      {this.state.btnLoader ? (
                        <span className="loader-btn"></span>
                      ) : (
                        `CREATE YOUR ACCOUT`
                      )}
                    </button>
                  </div>
                  <div className="body-text">
                    Already a member?
                    <Link className="link bold" to={{ pathname: "/login" }}>
                      Log In
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Register;
