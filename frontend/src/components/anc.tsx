import React, { Component } from "react";
import TextInput from "./Form/TextInput";
// import logo from "./../assets/logo-base.svg";
import { Link } from "react-router-dom";
import { handleEmail, isRequired } from "../components/Form/FormValdiations";
import { userLoginUrl, postMethod } from "../utils/apis";
import { getUserInfo } from "../utils/actionDispatcher";
import store from "../store";
import {
  setLocalStorage,
  getLocalStorage,
  removeFromLocalStorage,
} from "../utils/commonUtils";
import Constants, { errorMsg } from "../utils/Constants";
import { userLogOut } from "../utils/actionDispatcher";
import OfflineContent from "./common/OfflineContent";

export interface IErrors {
  [key: string]: string;
}
export interface IRegisterState {
  errors: IErrors;
  submitted?: boolean;
  email: string;
  password: string;
  isRemember: boolean;
  btnLoader?: boolean;
  [key: string]: any;
  isOffline?: boolean
}

class newLogin extends Component<any, IRegisterState> {
  constructor(props: any) {
    super(props);

    const errors: IErrors = {};
    this.state = {
      errors,
      email: "",
      password: "",
      isRemember: false,
    };
  }

  componentDidMount() {
    const user = getLocalStorage(Constants.rememberMe);
    const isUser = JSON.parse(JSON.stringify(user));
    const oldUser = JSON.parse(isUser);
    if (oldUser) {
      this.setState({
        email: oldUser.user,
        password: oldUser.password,
        isRemember: true,
      });
    }
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
  }

  private handleCheckBox = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      isRemember: e.target.checked,
    });
  };

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  private haveErrors(errors: IErrors) {
    let haveError: boolean = false;
    Object.keys(errors).forEach((key: string) => {
      if (errors[key].length > 0) {
        haveError = true;
      }
    });
    return haveError;
  }

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const { email, password } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          email: handleEmail(email),
          password: isRequired(password, "password"),
        },
      },
      () => {
        if (!this.haveErrors(this.state.errors)) {
          const form = { emailId: email.toLowerCase(), password };
          this.submitCall(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitCall = async (param: object): Promise<void> => {
    this.setState({ btnLoader: true });
    let res = await postMethod(userLoginUrl, param);
    if (
      res &&
      res.response &&
      (res.response.status === 404 || res.response.status === 401)
    ) {
      this.setState({ isOffline: false, btnLoader: false, errorFromApi: errorMsg.invalid });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ isOffline: false, btnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({ isOffline: false })
      const token = res.data.token;
      const adminData = res.data.data[0];
      setLocalStorage(Constants.userToken, token);
      store.dispatch(getUserInfo(adminData));
      if (this.state.isRemember) {
        setLocalStorage(
          Constants.rememberMe,
          JSON.stringify({
            user: this.state.email,
            password: this.state.password,
          })
        );
      } else {
        const user = getLocalStorage(Constants.rememberMe);
        if (user) {
          removeFromLocalStorage(Constants.rememberMe);
        }
      }
      this.postLogin();
    } else if (!res.status) {
      // console.log("no response");
      this.setState({  btnLoader: false, isOffline: true });
    }
  };

  postLogin = () => {
    this.setState({ btnLoader: false, submitted: true }, () => {
      setTimeout(() => {
        this.props.history.push("/user");
      }, 500);
    });
  };

  render() {
    const {isOffline} = this.state
    return (
      <div className="register-wrapper">
        <div className="container-fluid register-container">
          <div className="col-sm-12 col-xs-12">
            <div className="register-section-wrapper">
              <form className="form-horizontal" onSubmit={this.handleSubmit}>
              {isOffline ? <OfflineContent /> : null}
                <p className="login-reg-title">LOGIN</p>
                {/* {this.state.submitted ? (
                  <div className="form-success">
                    <span className="loading"> Login successful </span>
                  </div>
                ) : null} */}
                <TextInput
                  placeholder="Enter your email"
                  type="email"
                  label={"Email"}
                  onChange={this.handleChange}
                  name="email"
                  error={this.state.errors["email"]}
                  autoComplete="off"
                  value={this.state.email && this.state.email}
                />
                <TextInput
                  placeholder="password"
                  type="password"
                  label="Password"
                  onChange={this.handleChange}
                  error={this.state.errors["password"]}
                  name="password"
                  autoComplete="off"
                  value={this.state.password && this.state.password}
                />
                <div className="check-wrap">
                  <label className="custom-checkbox">
                    Remember password
                    <input
                      type="checkbox"
                      onChange={(e) => this.handleCheckBox(e)}
                      checked={this.state.isRemember && this.state.isRemember}
                    />
                    <span className="checkmark"></span>
                  </label>
                </div>
                <div
                  className="reg-error-parent"
                  style={{ marginBottom: "10px" }}
                >
                  <p className="reg-error">{this.state.errorFromApi}</p>
                </div>
                <div className="form-group">
                  <button className="register-btn full-width">
                    {" "}
                    {this.state.btnLoader ? (
                      <span className="loader-btn"></span>
                    ) : (
                      `LOGIN TO YOUR ACCOUT`
                    )}
                  </button>
                </div>
                <div className="body-text">
                  Don't have an account?
                  <Link className="link bold" to={{ pathname: "/register" }}>
                    Register
                  </Link>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default newLogin;
