import React from "react";

const TextInput = (props: any) => {
    const {name, type, onChange, autoComplete, required, onBlur } = props
    const onBlurDefault = (e: React.ChangeEvent<HTMLInputElement>) => {
    }
  return (
    <>
      <div className="input-icon-wrapper">
        <input
          className="form-control"
          type={type}
          required={required === "true" ? true : false}
          name={name}
          autoComplete={autoComplete}
          onChange={(e) => onChange(e)}
          onBlur={ onBlur ? (e) => onBlur(e) : onBlurDefault}
        />
        {/* <span className="fa fa-check icon-green"></span> */}
      </div>
    </>
  );
};

export default TextInput;
