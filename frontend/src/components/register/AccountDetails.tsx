import React from "react";

import Constants from "../../utils/Constants";

const UserFields = Constants.UserFields;

const AccountDetails = (props: any) => {
  const {
    userNameReqError,
    userNameExistsError,
    userEmailReqError,
    userEmailExistsError,
    userPasswordReqError,
    passwordsDoNotMatchError
  } = props;
  return (
    <>
      <div className="form-group">
        <label className="control-label">Username*</label>
        <div className="fields-holder">
          <div className="input-icon-wrapper">
            <input
              className="form-control"
              type="text"
              required
              name="username"
              autoComplete="off"
              onChange={e => props.userName(e, UserFields.userName)}
              onBlur={e => props.userNameExistsMethod(e, UserFields.userName)}
            />
            {/* <span className="fa fa-check icon-green"></span> */}
          </div>
          <p>
            {userNameReqError ? (
              <span className="reg-error">Please enter username</span>
            ) : null}
            {userNameExistsError ? (
              <span className="reg-error">
                Username already exists, try other
              </span>
            ) : null}
          </p>
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Email*</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="email"
            required
            name="email"
            autoComplete="off"
            onChange={e => props.userEmail(e, UserFields.userEmail)}
            onBlur={e => props.userEmailExistsMethod(e, UserFields.userEmail)}
          />
          <div className="checkbox">
            <label>
              <input
                type="checkbox"
                name="emailhiddenFromPublic"
                onChange={e =>
                  props.userEmailBoolean(e, UserFields.isEmailHiddenFromPublic)
                }
              />
              Make this field hidden from public
            </label>
          </div>
          <p>
            {userEmailReqError ? (
              <span className="reg-error">Please enter valid email</span>
            ) : null}
            {userEmailExistsError ? (
              <span className="reg-error">Email already registered</span>
            ) : null}
          </p>
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Password*</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="password"
            required
            name="password"
            autoComplete="off"
            onChange={e => props.userPassword(e, UserFields.userPassword)}
            onBlur={e => props.userPasswordValidate(e, UserFields.userPassword)}
            minLength={8}
          />
          <p>
            {userPasswordReqError ? (
              <span className="reg-error">
                Password should be a minimum of 8 characters
              </span>
            ) : null}
          </p>
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Confirm password*</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="password"
            required
            name="confirmPassword"
            autoComplete="off"
            onChange={e =>
              props.userConfirmPassword(e, UserFields.userConfirmPassword)
            }
          />
          <p>
            {passwordsDoNotMatchError ? (
              <span className="reg-error">Passwords do not match</span>
            ) : null}
          </p>
        </div>
      </div>
    </>
  );
};

export default AccountDetails;
