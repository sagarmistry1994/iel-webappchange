import React from "react";
import Select from "react-select";

import Constants from "../../utils/Constants";

const UserFields = Constants.UserFields;

const ProfileDetails = (props: any) => {

  const options = props.countryList.map((country: any) => {
    return { value: country.CountryID, label: country.CountryName };
  });
  return (
    <>
      <div className="form-group">
        <label className="control-label">Profile Name</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="text"
            name="profilename"
            autoComplete="off"
            onChange={(e) => props.userProfileName(e, UserFields.userProfileName)}
          />
          {/* <p>
            {userNameReqError ? (
              <span className="reg-error">Please enter username</span>
            ) : null}
            {userNameExistsError ? (
              <span className="reg-error">
                Username already exists, try other
              </span>
            ) : null}
          </p> */}
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Profile Picture</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="file"
            accept=".jpg,.jpeg,.png"
            name="profilepicture"
            autoComplete="off"
            onChange={e => props.handleDp(e.target.files)}
          />
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Gender</label>
        <div className="fields-holder">
          <label className="radio-inline">
            <input
              type="radio"
              name="gender"
              value="m"
              onChange={e => props.userGender(e, UserFields.userGender)}
            />
            <span className="gender">Male</span>
          </label>
          <label className="radio-inline">
            <input
              type="radio"
              name="gender"
              value="f"
              onChange={e => props.userGender(e, UserFields.userGender)}
            />
            <span className="gender">Female</span>
          </label>
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Country/Region</label>
        <div className="fields-holder">
          <Select options={options} onChange={e => props.userCountry(e)} />
        </div>
      </div>
    </>
  );
};

export default ProfileDetails;
