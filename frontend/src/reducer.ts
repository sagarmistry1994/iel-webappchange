import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import featuredTournamentsReducer from './reducers/featuredTournamentsReducer';
import ourGamesReducer from './reducers/ourGamesReducer';
import leaderBoardReducer from './reducers/leaderBoardReducer';
import allGamesReducer from './reducers/allGamesReducer';
import userReducer from './reducers/userReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  featuredTournaments: featuredTournamentsReducer,
  ourGames: ourGamesReducer,
  leaderBoard: leaderBoardReducer,
  allGames: allGamesReducer,
  user: userReducer, 
})

export type IAppState = ReturnType <typeof rootReducer>

export default rootReducer



