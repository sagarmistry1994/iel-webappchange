import {Actions} from "../utils/actions";
import { IFeaturedTournament } from "../utils/interfacePools";
import { IAction } from "../utils/actionDispatcher";

const initState: IFeaturedTournament[] | [] = [];

const allGamesReducer = (
  state = initState,
  action: IAction
): IFeaturedTournament[] => {
  switch (action.type) {
    case Actions.GET_ALL_GAMES:
      return action.payload;
    default:
      return state;
  }
};

export default allGamesReducer;