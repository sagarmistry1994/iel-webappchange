import {Actions} from '../utils/actions';
import { IFeaturedTournament } from '../utils/interfacePools';
import { IAction } from '../utils/actionDispatcher';


const initState : IFeaturedTournament[] | [] = [];

const featuredTournamentsReducer = (state = initState, action: IAction): IFeaturedTournament[] => {
  switch (action.type) {
    case Actions.GET_FEATURED_TOURNAMENTS:
      return action.payload
    default:
      return state;
  }
};

export default featuredTournamentsReducer;
