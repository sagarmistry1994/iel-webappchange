import { Actions } from "../utils/actions";
import { ILeaderBoardRow } from "../utils/interfacePools";
import { IAction } from "../utils/actionDispatcher";

const initState: ILeaderBoardRow[] | [] = [];

const leaderBoardReducer = (
  state = initState,
  action: IAction
): ILeaderBoardRow[] => {
  switch (action.type) {
    case Actions.GET_LEADERBOARD:
      return action.payload;
    default:
      return state;
  }
};

export default leaderBoardReducer;
