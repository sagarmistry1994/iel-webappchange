import {Actions} from '../utils/actions';
import { IOurGameCardOnHome } from '../utils/interfacePools';
import { IAction } from '../utils/actionDispatcher';


const initState : IOurGameCardOnHome[] | [] = [];

const ourGamesReducer = (state = initState, action: IAction): IOurGameCardOnHome[] => {
  switch (action.type) {
    case Actions.GET_OUR_GAMES:
      return action.payload
    default:
      return state;
  }
};

export default ourGamesReducer;
