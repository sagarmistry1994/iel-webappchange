import { Actions } from "../utils/actions";
import { IUserProfileNewDetails } from "../utils/interfacePools";
import { IAction } from "../utils/actionDispatcher";

const initState: IUserProfileNewDetails | {} = {};

const userReducer = (state = initState, action: IAction): IUserProfileNewDetails => {
  switch (action.type) {
    case Actions.GET_USER_INFO:
      return action.payload;
    case Actions.USER_LOGOUT:
      return {};
    default:
      return state;
  }
};

export default userReducer;
