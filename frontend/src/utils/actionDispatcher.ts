import { Actions } from "./actions";
import {
  IFeaturedTournament,
  IOurGameCardOnHome,
  ILeaderBoardRow,
  IUserProfileNewDetails,
} from "./interfacePools";

const makeAction = <T extends Actions, P>(type: T) => (payload: P) => {
  return {
    type,
    payload,
  };
};

export const getFeaturedTournaments = makeAction<
  Actions.GET_FEATURED_TOURNAMENTS,
  IFeaturedTournament[]
>(Actions.GET_FEATURED_TOURNAMENTS);

export const getOwnGames = makeAction<
  Actions.GET_OUR_GAMES,
  IOurGameCardOnHome[]
>(Actions.GET_OUR_GAMES);

export const getLeaderBoardDetails = makeAction<
  Actions.GET_LEADERBOARD,
  ILeaderBoardRow[]
>(Actions.GET_LEADERBOARD);

export const getAllGames = makeAction<
  Actions.GET_ALL_GAMES,
  IFeaturedTournament[]
>(Actions.GET_ALL_GAMES);

export const getUserInfo = makeAction<Actions.GET_USER_INFO, IUserProfileNewDetails>(
  Actions.GET_USER_INFO
);

export const userLogOut = makeAction<Actions.USER_LOGOUT,''>(
  Actions.USER_LOGOUT
);

interface IStringMap<T> {
  [key: string]: T;
}
type IAnyFunction = (...args: any[]) => any;
type IActionUnion<A extends IStringMap<IAnyFunction>> = ReturnType<A[keyof A]>;

const actions = {
  getFeaturedTournaments,
  getOwnGames,
  getLeaderBoardDetails,
  getAllGames,
  getUserInfo,
  userLogOut
};

export type IAction = IActionUnion<typeof actions>;
