import Constants from "./Constants";

interface IErrors {
  [key: string]: string;
}

export const setLocalStorage = (keyName: string, value: any) => {
  localStorage.setItem(keyName, value);
};

export const getLocalStorage = (keyName: string) => {
  return localStorage.getItem(keyName);
};

export const removeFromLocalStorage = (keyName: string) => {
  localStorage.removeItem(keyName);
};

export const validateFormFields = {
  validateEmail(email: string): boolean {
    // const regEx = /^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/;
    const regEx = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (email.match(regEx)) {
      return true;
    } else {
      return false;
    }
  },
};

export const isUserLogged = (): boolean => {
  const userLoggedIn =
    // getLocalStorage(Constants.userProfile) &&
    getLocalStorage(Constants.userToken);
  if (userLoggedIn) {
    return true;
  } else {
    return false;
  }
};

export const haveErrors = (errors: IErrors): boolean => {
  let haveError: boolean = false;
  Object.keys(errors).forEach((key: string) => {
    if (errors[key].length > 0) {
      haveError = true;
    }
  });
  return haveError;
};

export const dateValidation = (param: string): boolean => {
  const iDate = param.split("-");
  let c = new Date();
  let date = c.getDate().toString();
  let month = (c.getMonth() + 1).toString();
  let year = c.getFullYear().toString();

  if (iDate[0] <= year && iDate[1] <= month && iDate[2] <= date) {
    return false;
  } else {
    return true;
  }
};

export const validatePhoneNumber = (elementValue: string): boolean => {
  var pattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
  // var pattern = /^(1-?)?(\\([2-9]\\d{2}\\)|[2-9]\\d{2})-?[2-9]\\d{2}-?\\d{4}$/;
  // var pattern = /^+(d{1,2}))?[- ](d{3})[- ](d{3})[- ](d{3,4})?$/;
  // console.log(pattern.test(elementValue));
  return pattern.test(elementValue);
};

export const IsUserProfileLocalStorage = (): any => {
  const rawData = getLocalStorage(Constants.userProfile);
  let userData: any = "";
  if (rawData && rawData) {
    userData = JSON.parse(rawData);
    let userId = userData[`${Constants.UserID}`];
    return userId;
  }
  else{
    return null
  }
};
