export interface IFeaturedTournament {
  TournamentID?: number;
  TournamentName?: string;
  [key: string] : any;
  players: IPlayers[];
  BulletsHit?: any,
  BulletHitPerc?: any,
  kills?: any,
  Deaths?: any,
  Cash?: any,
}

export interface IPlayers {
  PlayerName: string;
  Score?: number;
  [key: string] : any;
}

export interface IOurGameCardOnHome {
  GameID: number;
  GameName: string;
  GameShortDesc: string;
  GameDesc: string;
  GameTypeID?: number;
  PlatformID?: number;
  HomeBannerImageLink?: string;
  HomeBannerText?: string;
  IsEnabled?: boolean;
  funcasfaf?: Function;
}

export interface IAction {
  type: string;
  payload: object | string | number | "";
}

export interface ILeaderBoardRow {
  Rank?: number;
  Player?: string;
  LastCompletedLevel?: number;
  Time?: number;
  BulletsTotal?: number;
  BulletsHit?: number;
  BulletHitPerc?: string;
  Kills?: number;
  Deaths?: number;
  Score?: number;
  leaders?: {};
  [key: string] : any;
  ProfileDisplayName?: string;
}

export interface IGameCardOnLeaderBoard {
  rows?: interMediateObject | {};
  leaderRows?: ILeaderBoardRow[] | [];
}

export interface interMediateObject {
  leaderBoard: ILeaderBoardRow[];
  history?: object;
}

export interface IRegisterState {
  isAccDetails: boolean;
  isUserDetails: boolean;
  isSocialDetails: boolean;
  userName: string;
  userEmail: string;
  userPassword: string;
  confirmPassword: string;
  isUserEmailPublic?: number;
  passwordsDontMatch: boolean;
  userNameReq: boolean;
  userEmailReq: boolean;
  userPasswordReq: boolean;
  userNameExistsError: boolean;
  userEmailExistsError: boolean;
  isSubmitDisabled: boolean;
  selectedFile?: Object;
  allCountries: country[];
  userProfileName?: string;
  userGender?: string;
  userCountry?: number;
  userFb?: string;
  userTwitter?: string;
  userWebsite?: string;
  profileImgUrl?: string;
  userGooglePlus?: string;
  showSuccessMsg?: boolean;
}

interface country {
  CountryID: number;
  CountryName: string;
}

export interface IUserProfile {
  UserID?: number;
  UserName?: string;
  EmailID?: string;
  IsHiddenEmailID?: boolean;
  Pwd?: string;
  ProfileDisplayName?: string;
  ProfilePictureUrl?: string;
  Gender?: string;
  CountryID?: number;
  FacebookProfile?: string;
  TwitterProfile?: string;
  GooglePlusProfile?: string;
  WebsiteUrl?: string;
  RoleID?: null;
  IsEmailVerified?: false;
  IsEnabled?: true;
  CreatedOn?: string;
  OTP?: string;
}

export interface ILoginState {
  userName: string;
  userPassword: string;
  isFormInvalid: boolean;
  showSuccessMsg: boolean;
  isInvalidCredentials: boolean;
  [key: string]: any;
}

export interface IVerificationEmail {
  userName: string;
  isScreen1: boolean;
  otp: number | null;
  otpErrorMsg: string;
  userNameErroMsg: string;
  showSuccessMsg: boolean;
  [key: string]: any;
}


export interface IUserProfileNewDetails{
  UserID?: number;
  EmailID?: number;
  UserName?: string;
  ProfileDisplayName?: string;
  ProfilePictureUrl?: string;
  FacebookProfile?: string;
  TwitterProfile?: string;
  InstagramProfile?: string;
  SteamProfile?: string;
  DiscordProfile?: string;
  XboxProfile?: string;
  PsProfile?: string;
  StateShortCode?: string;
  CountryName?: string;
  About?: string;
  Followers?: string;
  InGameUserID?: number;
}
